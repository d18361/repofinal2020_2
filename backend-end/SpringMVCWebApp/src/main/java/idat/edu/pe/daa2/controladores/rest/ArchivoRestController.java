package idat.edu.pe.daa2.controladores.rest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import idat.edu.pe.daa2.jpa.modelo.Archivo;
import idat.edu.pe.daa2.jpa.servicios.ArchivoServicio;

@RestController
@RequestMapping("/rest/archivo")
public class ArchivoRestController {

	@Autowired
	private ArchivoServicio servicio;

	@PostMapping
	public ResponseEntity<String> cargarArchivo(@RequestPart("nuevoArchivo") MultipartFile archivo) {
		try {
			
			if (null == archivo.getOriginalFilename() || "".equalsIgnoreCase(archivo.getOriginalFilename())
					|| null == archivo.getBytes()) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie archivo");
			}

			byte[] bytes = archivo.getBytes();
			Archivo nuevoArchivo = new Archivo(bytes);
			servicio.crear(nuevoArchivo); // creacion en la base de datos
			
			// guardando localmente
			Path path = Paths.get(archivo.getOriginalFilename());
			System.out.println("Path:" + path);
			Files.write(path, bytes);
			System.out.println(path.getFileName());

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return new ResponseEntity<>("Archvio grabado con éxito", HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public ResponseEntity<Object> buscarPorID(@PathVariable("id") int id) {
		Archivo archivo = servicio.buscarPorID(id);

		if (archivo == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"Archivo no encontrada, id proporcionado no es correcto !!!");

		return new ResponseEntity<>(archivo, HttpStatus.OK);
	}

}
