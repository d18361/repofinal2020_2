package idat.edu.pe.daa2.controladores.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import idat.edu.pe.daa2.jpa.modelo.Funciones;
import idat.edu.pe.daa2.jpa.servicios.FuncionesServicio;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/rest/funciones")
public class FuncionesRestController {

	@Autowired
	private FuncionesServicio servicio;

	@GetMapping
	public ResponseEntity<Object> buscarTodo() {
		List<Funciones> listaFunciones = servicio.buscarTodo();
		return new ResponseEntity<Object>(listaFunciones, HttpStatus.OK);

	}

	@GetMapping(value = "/cines/{idCine}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Object> buscarFucnionesPorCine(@PathVariable("idCine") String idCine) {
		List<Funciones> listaFunciones = servicio.buscarTodo(); // busco todas las funciones !!!
		if (idCine != null && idCine != "")
			listaFunciones = listaFunciones.stream()
					.filter((f) -> idCine.equalsIgnoreCase(f.getSala().getSede().getCine().getIdCine().toString())) // filtros
																													// con
																													// Streams!!!
					.collect(Collectors.toList());
		return new ResponseEntity<Object>(listaFunciones, HttpStatus.OK);

	}

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public ResponseEntity<Object> buscarPorId(@PathVariable("id") int id) {
		Funciones funcion = servicio.buscarPorID(id);

		if (funcion == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"Funcion no encontrada, id proporcionado no es correcto !!!");

		return new ResponseEntity<>(funcion, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Object> crear(@RequestBody Funciones funcion) {
		servicio.crear(funcion);
		Map<String, String> mapa = new HashMap<>();
		mapa.put("codigo", "OK");
		mapa.put("mensaje", "Funcion creada correctamente en BD");
		return new ResponseEntity<Object>(mapa, HttpStatus.CREATED);

	}

	@PutMapping(value = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Object> actualizar(@PathVariable("id") int id, @RequestBody Funciones funcionActualizar) {
		servicio.actualizar(funcionActualizar);
		Map<String, String> mapa = new HashMap<>();
		mapa.put("codigo", "OK");
		mapa.put("mensaje", "Funcion actualizada correctamente en BD");

		return new ResponseEntity<Object>(mapa, HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Object> borrar(@PathVariable("id") int id) {
		Funciones funcion = servicio.buscarPorID(id);

		if (funcion == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"Funcion no encontrada, id proporcionado no es correcto,imposible eliminar !!!");

		servicio.borrarPorID(id);
		Map<String, String> mapa = new HashMap<>();
		mapa.put("codigo", "OK");
		mapa.put("mensaje", "Se elimino correctamente la funcion con id : " + id);

		return new ResponseEntity<Object>(mapa, HttpStatus.OK);
	}
	
	@GetMapping(value = "/peliculas", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Object> buscarFucnionesPorPelicula(@RequestParam("opcion") String opcion,
			@RequestParam("pelicula") String pelicula) {
		List<Funciones> listaFunciones = null;
		switch (opcion) {
		case "1":
			listaFunciones = servicio.buscarPorNombrePelicula(pelicula);
			break;
		case "2":
			listaFunciones = servicio.buscarPorLiteralPelicula(pelicula);
			break;
		default:
			listaFunciones = servicio.buscarPorPeliculaNativo(pelicula);
			break;
		}
		return new ResponseEntity<Object>(listaFunciones, HttpStatus.OK);
	}
}
