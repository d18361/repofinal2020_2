package idat.edu.pe.daa2.controladores.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import idat.edu.pe.daa2.jpa.modelo.Sala;
import idat.edu.pe.daa2.jpa.modelo.Sede;
import idat.edu.pe.daa2.jpa.servicios.SalaServicio;

@RestController
@RequestMapping("/rest/salas")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SalaRestController {

	@Autowired
	private SalaServicio servicio;
	
	@GetMapping
	public ResponseEntity<Object> buscarTodo(@RequestParam(value = "sede", required = false) String sede) {
		List<Sala> listaSalas = servicio.buscarTodo();
		if (sede != null && sede != "")
			listaSalas = listaSalas.stream().filter((f) -> sede.equalsIgnoreCase(f.getSede().getIdSede().toString()))
					.collect(Collectors.toList());
		return new ResponseEntity<Object>(listaSalas, HttpStatus.OK);

	}
}
