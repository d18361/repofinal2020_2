package idat.edu.pe.daa2.controladores.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import idat.edu.pe.daa2.jpa.modelo.Sede;
import idat.edu.pe.daa2.jpa.servicios.SedeServicio;

@RestController
@RequestMapping("/rest/sedes")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SedeRestController {

	@Autowired
	private SedeServicio servicio;

	@GetMapping
	public ResponseEntity<Object> buscarTodo(@RequestParam(value = "cine", required = false) String cine) {
		List<Sede> listaSedes = servicio.buscarTodo();
		if (cine != null && cine != "")
			listaSedes = listaSedes.stream().filter((f) -> cine.equalsIgnoreCase(f.getCine().getIdCine().toString()))
					.collect(Collectors.toList());
		return new ResponseEntity<Object>(listaSedes, HttpStatus.OK);

	}
}
