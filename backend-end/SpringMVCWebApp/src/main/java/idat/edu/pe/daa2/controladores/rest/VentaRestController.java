package idat.edu.pe.daa2.controladores.rest;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import idat.edu.pe.daa2.controladores.rest.dto.ReporteDTO;
import idat.edu.pe.daa2.jpa.servicios.VentaServicio;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/rest/ventas")
public class VentaRestController {

	@Autowired
	private VentaServicio servicio;

	public VentaRestController() {
		// TODO Auto-generated constructor stub
	}

	@GetMapping(value = "/reportes", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Object> buscarVentas(@RequestParam("opcion") String opcion) {
		List<Object[]> listaVentas = null;

		switch (opcion) {
		case "1":
			listaVentas = servicio.obtenerVentasPorFecha();
			break;
		case "2":
			listaVentas = servicio.obtenerVentasPorUsuario();
			break;
		default:
			listaVentas = servicio.obtenerVentasPorUbicacion();
			break;
		}
		ReporteDTO reporte = convertir(listaVentas);
		//return new ResponseEntity<Object>(listaVentas, HttpStatus.OK);
		return new ResponseEntity<Object>(reporte, HttpStatus.OK);
	}

	private ReporteDTO convertir(List<Object[]> listaVentas) {
		// TODO Auto-generated method stub
		String[] etiquetas = new String[listaVentas.size()];
		BigDecimal[] valores = new BigDecimal[listaVentas.size()];
		Object[] o = null;
		for (int i = 0; i < listaVentas.size(); i++) {
             o =  listaVentas.get(i);
             etiquetas[i] = o[0].toString();
             valores[i] = new BigDecimal(o[1].toString());
		}
		return new ReporteDTO(etiquetas,valores);
	}
}
