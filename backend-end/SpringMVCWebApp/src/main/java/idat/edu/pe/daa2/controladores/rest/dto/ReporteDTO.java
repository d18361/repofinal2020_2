package idat.edu.pe.daa2.controladores.rest.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReporteDTO implements Serializable {

	
	private String[] etiquetas;
	
	private BigDecimal[] valores;
	
	public ReporteDTO() {
		// TODO Auto-generated constructor stub
	}

	public ReporteDTO(String[] etiquetas, BigDecimal[] valores) {
		super();
		this.etiquetas = etiquetas;
		this.valores = valores;
	}

	public String[] getEtiquetas() {
		return etiquetas;
	}

	public void setEtiquetas(String[] etiquetas) {
		this.etiquetas = etiquetas;
	}

	public BigDecimal[] getValores() {
		return valores;
	}

	public void setValores(BigDecimal[] valores) {
		this.valores = valores;
	}

	
}
