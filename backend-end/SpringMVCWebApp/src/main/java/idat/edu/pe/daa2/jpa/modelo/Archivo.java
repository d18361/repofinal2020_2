package idat.edu.pe.daa2.jpa.modelo;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "archivo")
public class Archivo implements Serializable {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer idArchivo;
	
	/* Tipos de datos para columnas binarias en MySQL
		0 < length <=      255 bytes  -->  `TINYBLOB`
    	255 < length <=    65535  -->  `BLOB`
    	65535 < length <= 16777215  -->  `MEDIUMBLOB`
    	16777215 < length <=    2³¹-1  -->  `LONGBLOB`
	*/
	@Lob
	@Basic(fetch = FetchType.LAZY) // buena practica LAZY = ocioso fetch = obtener , EAGER
	@Column(name = "binario", columnDefinition = "MEDIUMBLOB",nullable=true)
	private byte[] archivo;

	public Archivo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Archivo(byte[] archivo) {
		super();
		this.archivo = archivo;
	}

	public Integer getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Integer idArchivo) {
		this.idArchivo = idArchivo;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}
	
	
	
	
}
