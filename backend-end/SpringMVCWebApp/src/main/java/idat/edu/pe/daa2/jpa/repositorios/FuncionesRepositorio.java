package idat.edu.pe.daa2.jpa.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;

import idat.edu.pe.daa2.jpa.modelo.Funciones;


public interface FuncionesRepositorio extends CrudRepository<Funciones, Integer> {

	@Query(value="SELECT f FROM Funciones f WHERE f.pelicula.nombre = :nombrePelicula")
	public List<Funciones> buscarFuncionesPorPelicula(@Param("nombrePelicula") String pelicula);
	
	
	@Query(value="SELECT f FROM Funciones f WHERE f.pelicula.nombre LIKE CONCAT(?1, '%')")
	public List<Funciones> buscarFuncionesPorPeliculaLiteral(String pelicula);
	
	
	@Query(value="SELECT f.* FROM Funciones f,pelicula p WHERE f.pelicula = p.idPelicula and p.nombre = ?1",nativeQuery = true)
	public List<Funciones> buscarFuncionesPorPeliculaNativo(String pelicula);
	
	
}
