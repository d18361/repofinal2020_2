package idat.edu.pe.daa2.jpa.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import idat.edu.pe.daa2.jpa.modelo.Venta;

public interface VentaRepositorio extends CrudRepository<Venta, Integer> {

	@Query(value="SELECT function('date_format', v.fecha,'%d-%m-%Y') as fecha,SUM(v.total) FROM Venta v GROUP BY v.fecha")
	public List<Object[]> obtenerVentasPorFecha();
	
	@Query(value="SELECT v.user,SUM(v.total) FROM Venta v GROUP BY v.user")
	public List<Object[]> obtenerVentasPorUsuario();
	
	@Query(value="SELECT v.ubicacion,SUM(v.total) FROM Venta v GROUP BY v.ubicacion ")   //JPQL
	public List<Object[]> obtenerVentasPorUbicacion();  //cada fila de la consulta representa un arreglo de 2 elementos!!!
}
