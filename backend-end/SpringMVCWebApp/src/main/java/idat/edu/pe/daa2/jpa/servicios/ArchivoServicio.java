package idat.edu.pe.daa2.jpa.servicios;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import idat.edu.pe.daa2.jpa.modelo.Archivo;
import idat.edu.pe.daa2.jpa.repositorios.ArchivoRepositorio;

@Service
@Transactional
public class ArchivoServicio {

	@Autowired
	private ArchivoRepositorio repositorio;
	
	public void crear(Archivo archivo)
	{
		repositorio.save(archivo);
	}
	
	public List<Archivo> buscar()
	{
		return (List<Archivo>) repositorio.findAll();
	}
	
	public Archivo buscarPorID(Integer id)
	{
		return repositorio.findById(id).get();
	}
	

	public void borrarPorID(Integer id) {
		repositorio.deleteById(id);
	}
}
