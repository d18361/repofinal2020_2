package idat.edu.pe.daa2.jpa.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import idat.edu.pe.daa2.jpa.repositorios.VentaRepositorio;

@Service
@Transactional
public class VentaServicio {

	@Autowired
	private VentaRepositorio repositorio;
	
	public VentaServicio() {
		// TODO Auto-generated constructor stub
	}

	public List<Object[]> obtenerVentasPorFecha()
	{
		return repositorio.obtenerVentasPorFecha();
	}
	
	public List<Object[]> obtenerVentasPorUsuario()
	{
		return repositorio.obtenerVentasPorUsuario();
	}
	
	public List<Object[]> obtenerVentasPorUbicacion()
	{
		return repositorio.obtenerVentasPorUbicacion();
	}
}
