CREATE TABLE `carteleradb2`.`ventas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATE NOT NULL,
  `mtototal` DECIMAL(10,4) NOT NULL,
  `usuario` VARCHAR(45) NOT NULL,
  `ubicacion` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));