import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { CarritoDetalleComponent } from './components/carrito-detalle/carrito-detalle.component';
import { FuncionesDetailComponent } from './components/funciones-detail/funciones-detail.component';
import { FuncionesListComponent } from './components/funciones-list/funciones-list.component';
import { FuncionesNewComponent } from './components/funciones-new/funciones-new.component';
import { ReportesVentasComponent } from './components/reportes-ventas/reportes-ventas.component';

const routes: Routes = [
  { path: '', redirectTo: 'funcionesWeb', pathMatch: 'full' },
  { path: 'funcionesWeb', component: FuncionesListComponent },
  { path: 'funcionesWeb/actualizar/:id', component: FuncionesDetailComponent },
  { path: 'funcionesWeb/crear', component: FuncionesNewComponent },
  { path: 'funcionesWeb/funciones/cine/:idCine', component: FuncionesListComponent },
  { path: 'funcionesWeb/funciones/peliculas/:nombrePelicula', component: FuncionesListComponent },
  { path: 'funcionesWeb/carrito-detalle', component: CarritoDetalleComponent }, 
  { path: 'funcionesWeb/reportes', component: ReportesVentasComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
