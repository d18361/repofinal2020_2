import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FuncionesListComponent } from './components/funciones-list/funciones-list.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FuncionesDetailComponent } from './components/funciones-detail/funciones-detail.component';
import { FormsModule } from '@angular/forms';
import { FuncionesNewComponent } from './components/funciones-new/funciones-new.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { CompraFuncionesComponent } from './components/compra-funciones/compra-funciones.component';
import { MenuCinesComponent } from './components/menu-cines/menu-cines.component';
import { BuscarFuncionesComponent } from './components/buscar-funciones/buscar-funciones.component';
import { CarritoEstadoComponent } from './components/carrito-estado/carrito-estado.component';
import { CarritoDetalleComponent } from './components/carrito-detalle/carrito-detalle.component';
//import { ChartsModule } from 'ng2-charts/public_api';
import { ChartsModule } from 'ng2-charts';  
//import { ChartModule } from 'angular-highcharts';
import { ReportesVentasComponent } from './components/reportes-ventas/reportes-ventas.component';
//import { ChartsModule } from 'ng2-charts/ng2-charts' 
//Simport { ChartModule } from 'angular-highcharts';  
@NgModule({
  declarations: [  
    AppComponent,
    FuncionesListComponent,
    FuncionesDetailComponent,
    FuncionesNewComponent,
    CompraFuncionesComponent,
    MenuCinesComponent,
    BuscarFuncionesComponent,
    CarritoEstadoComponent,
    CarritoDetalleComponent,
    ReportesVentasComponent    
  ],  
  imports: [    
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,   
    NgxPaginationModule,
    ChartsModule    
       
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }   
