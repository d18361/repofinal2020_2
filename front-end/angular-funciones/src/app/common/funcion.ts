import { Pelicula } from "./pelicula";
import { Sala } from "./sala";

export class Funcion {

     idFunciones!: string;
     horaInicio!: string;
     horaFin!: string;
     precio!: string;
     pelicula!: Pelicula;
     sala!: Sala;
     
     constructor() { 
          this.pelicula = new Pelicula();
          this.sala = new Sala();
     }

}
