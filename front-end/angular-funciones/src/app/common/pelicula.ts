export class Pelicula {

    idPelicula!: number;
    nombre!: string;
    duracion!:string;
    clasificacion!:string;
    idioma!: string;
    genero!: string;
    formato!: string;
    sinopsis!: string;
}
