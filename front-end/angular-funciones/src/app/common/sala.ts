import { Sede } from "./sede";

export class Sala {
    idSala!: number;
    nombre!: string;
    capacidad!: number;
    sede!: Sede;
}
