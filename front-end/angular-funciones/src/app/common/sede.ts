import { Categoria } from "./categoria";
import { Cine } from "./cine";

export class Sede {
    idSede!: number;
    nombre!: string;
    direccion!: string;
    categoria !: Categoria;
    cine !: Cine;
}
