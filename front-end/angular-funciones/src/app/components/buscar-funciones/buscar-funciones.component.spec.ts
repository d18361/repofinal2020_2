import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarFuncionesComponent } from './buscar-funciones.component';

describe('BuscarFuncionesComponent', () => {
  let component: BuscarFuncionesComponent;
  let fixture: ComponentFixture<BuscarFuncionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscarFuncionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscarFuncionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
