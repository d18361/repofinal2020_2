import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FuncionService } from 'src/app/services/funcion-service';

@Component({
  selector: 'app-buscar-funciones',
  templateUrl: './buscar-funciones.component.html',
  styleUrls: ['./buscar-funciones.component.css']
})
export class BuscarFuncionesComponent implements OnInit {

  constructor(

              private router: Router) { }

  ngOnInit(): void {
  }

  buscarPorNombrePelicula(nombrePelicula : string)
  {
    console.log(`filtrar funciones por pelicula ...: ${nombrePelicula}`);
    this.router.navigate([`funcionesWeb/funciones/peliculas/${nombrePelicula}`]);
  }

}
