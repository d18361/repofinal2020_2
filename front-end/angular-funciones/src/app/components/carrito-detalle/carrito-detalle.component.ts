import { Component, OnInit } from '@angular/core';
import { Funcion } from 'src/app/common/funcion';
import { CarritoService } from 'src/app/services/carrito-service';

@Component({
  selector: 'app-carrito-detalle',
  templateUrl: './carrito-detalle.component.html',
  styleUrls: ['./carrito-detalle.component.css']
})
export class CarritoDetalleComponent implements OnInit {

  funcionesCompradas: Funcion[] = [];
  precioTotal: number = 0;
  cantidadTotal: number = 0;


  constructor(private carrito : CarritoService) { }
  
  ngOnInit(): void {
    this.listarDetalles();
  }

  listarDetalles() {

    
    this.funcionesCompradas = this.carrito.funcionesComprar;

  
    this.carrito.cantidadTotal.subscribe(
      data => this.cantidadTotal = data
    );

    
    this.carrito.precioTotal.subscribe( 
      data => this.precioTotal = data
    );

   
  }
}
