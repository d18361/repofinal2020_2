import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarritoEstadoComponent } from './carrito-estado.component';

describe('CarritoEstadoComponent', () => {
  let component: CarritoEstadoComponent;
  let fixture: ComponentFixture<CarritoEstadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarritoEstadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarritoEstadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
