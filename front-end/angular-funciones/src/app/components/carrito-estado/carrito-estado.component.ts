import { Component, OnInit } from '@angular/core';
import { CarritoService } from 'src/app/services/carrito-service';

@Component({
  selector: 'app-carrito-estado',
  templateUrl: './carrito-estado.component.html',
  styleUrls: ['./carrito-estado.component.css']
})
export class CarritoEstadoComponent implements OnInit {
  
  total : any;

  cantidad : any;

  constructor( private servicioCarrito : CarritoService) { 
  }

  actualizarValores()
  {
    //Subscribimos al total de funciones compradas !!
    this.servicioCarrito.cantidadTotal.subscribe(data => this.cantidad = data);
    //Subscribimos a la cantidad total de funciones compradas !!
    this.servicioCarrito.precioTotal.subscribe(data => this.total = data)
  }

  ngOnInit(): void {
    this.actualizarValores();
  }


}
