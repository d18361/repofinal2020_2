import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompraFuncionesComponent } from './compra-funciones.component';

describe('CompraFuncionesComponent', () => {
  let component: CompraFuncionesComponent;
  let fixture: ComponentFixture<CompraFuncionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompraFuncionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompraFuncionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
