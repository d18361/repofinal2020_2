import { Component, Input, OnInit } from '@angular/core';
import { Funcion } from 'src/app/common/funcion';

@Component({
  selector: 'app-compra-funciones',
  templateUrl: './compra-funciones.component.html',
  styleUrls: ['./compra-funciones.component.css']
})
export class CompraFuncionesComponent implements OnInit {

  @Input() funcionesComprar : any ;

  constructor() { }

  ngOnInit(): void {
   
  }

}
