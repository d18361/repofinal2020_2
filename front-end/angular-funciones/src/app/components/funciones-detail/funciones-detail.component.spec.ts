import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FuncionesDetailComponent } from './funciones-detail.component';

describe('FuncionesDetailComponent', () => {
  let component: FuncionesDetailComponent;
  let fixture: ComponentFixture<FuncionesDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FuncionesDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FuncionesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
