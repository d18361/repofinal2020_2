import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cine } from 'src/app/common/cine';
import { Funcion } from 'src/app/common/funcion';
import { Pelicula } from 'src/app/common/pelicula';
import { FuncionService } from 'src/app/services/funcion-service';
import { PeliculaService } from 'src/app/services/pelicula-service';

@Component({
  selector: 'app-funciones-detail',
  templateUrl: './funciones-detail.component.html',
  styleUrls: ['./funciones-detail.component.css']
})
export class FuncionesDetailComponent implements OnInit {
   
 funcionActual !: Funcion ;
 mensaje !: string ;
 peliculas : Pelicula[] | undefined; 


  constructor(private funcionServicio : FuncionService , 
              private route: ActivatedRoute,
              private router: Router,
              private peliculaServicio : PeliculaService) { }
  obetenerFuncion(id: string | null )
  {
    console.log("Invocando servicio rest-findByID");
     this.funcionServicio.getFuncionByID(id).subscribe(data =>{
      console.log(data); 
      this.funcionActual = data;
      console.log("id obtenido:" + this.funcionActual.pelicula.idPelicula);
      this.peliculaServicio.getPeliculasList().subscribe(
        data =>{
                 console.log(data); 
                 this.peliculas = data;
                 
     }
      );
    },  
    error => {
      console.log(error);
      this.mensaje = "No se puede acceder a la funcion !!!!";   
    })  
  }      
  
  actualizarFuncion()
  {
    console.log("Invocando servicio rest-updateByID");
     this.funcionServicio.updateFuncion(this.funcionActual.idFunciones,this.funcionActual).subscribe(data =>{
      console.log(data); 
      this.mensaje = "Se actualizo correctamente la funcion";   
    },
    error => {
      console.log(error);
      this.mensaje = "Hubo un error actualizando la funcion";   
    })  
  }  
  regresar()
  {
    console.log("A la busqueda de funciones de cine");
    this.router.navigate(['/funcionesWeb']);
  }

  compararNombresPelicula(pelicula1 : Pelicula , pelicula2 : Pelicula)
  {
    console.log("En el comparador!!")
    console.log("pelicula1 : " + pelicula1.idPelicula +  " nombre :" +pelicula1.nombre);
    console.log("pelicula2 : " + pelicula2.idPelicula +  " nombre :" +pelicula2.nombre);
    if (pelicula1==null || pelicula2==null) 
    { 
        console.log("Retornando falso!")
        return false;  
    } 
    let flag = pelicula1.idPelicula===pelicula2.idPelicula ;
    console.log("retorando : " + flag);
    return flag;  
  }

  ngOnInit(): void {
    this.mensaje = "";
    this.obetenerFuncion(this.route.snapshot.paramMap.get('id'));
    
  }
}
