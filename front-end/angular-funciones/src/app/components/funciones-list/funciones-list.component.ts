import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Funcion } from 'src/app/common/funcion';
import { CarritoService } from 'src/app/services/carrito-service';
import { FuncionService } from 'src/app/services/funcion-service';
import { ChartsModule } from 'ng2-charts';
@Component({
  selector: 'app-funciones-list',
  templateUrl: './funciones-list.component.html',
  styleUrls: ['./funciones-list.component.css']
})
export class FuncionesListComponent implements OnInit {
  
  funciones:any; 
  funcionesAComprar: Funcion[] = [];
  page = 1;
  count = 0;
  idCine : any;
  nomPelicula : any;

  handlePageChange(event: number ) {
    this.page = event;
  }  

  comprarFuncion(funcionComprada : Funcion)
  {
     this.funcionesAComprar.push(funcionComprada);
     this.carrito.agregarFuncion(funcionComprada);
  }

  constructor(private funcionServicio : FuncionService , 
              private route: ActivatedRoute,
              private router: Router,
              private carrito: CarritoService) { }
   
  listarFunciones(){
    console.log('invocando funcion');
   const tieneIdCine: boolean = this.route.snapshot.paramMap.has('idCine');
   if (tieneIdCine) {
    this.idCine = this.route.snapshot.paramMap.get('idCine');  
    console.log("Invocano servicio rest de busqueda de funciones con filtro por cine");
    this.funcionServicio.getFuncionesPorCineList(this.idCine).subscribe(data =>{
      console.log(data);  
      console.table(data);
      this.funciones = data;
    })
  }
  else {

      const tieneNomPelicula: boolean = this.route.snapshot.paramMap.has('nombrePelicula');
      if (tieneNomPelicula)
      {
        this.nomPelicula = this.route.snapshot.paramMap.get('nombrePelicula');
        console.log("Invocando al servicio rest de busqueda por nombre literal");
        this.buscarPorNombrePelicula(this.nomPelicula);
      }
      else
      {
          console.log("Invocando servicio rest de busqueda total de funciones"); 
          this.funcionServicio.getFuncionesList().subscribe(data =>{
          console.log(data);  
          console.table(data);
          this.funciones = data;
        })  
      }
  }  
  }


  buscarPorNombrePelicula(nombrePelicula : string)
  {
    this.funcionServicio.getFuncionesPorPeliculaList(nombrePelicula).subscribe(data =>{
      console.log(data);  
      console.table(data);
      this.funciones = data;
    })  
  }

  borrarFuncion(id : string) {
     console.log("Invocando servicio rest-delete");
     this.funcionServicio.deleteFuncionByID(id).subscribe(data =>{
      console.log(data); 
      this.ngOnInit();
      this.router.navigate(['/funcionesWeb']);
      
    },
    error => {
      console.log(error);
    })  
    
  }


  ngOnInit(): void {
  
    this.route.paramMap.subscribe(() => {
      console.log("Inicio");
      this.listarFunciones();
      console.log("Continuando...");
      console.log("fin");
    });

  }
}
