import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FuncionesNewComponent } from './funciones-new.component';

describe('FuncionesNewComponent', () => {
  let component: FuncionesNewComponent;
  let fixture: ComponentFixture<FuncionesNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FuncionesNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FuncionesNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
