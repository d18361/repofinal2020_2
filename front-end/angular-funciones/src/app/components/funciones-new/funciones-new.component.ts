import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cine } from 'src/app/common/cine';
import { Funcion } from 'src/app/common/funcion';
import { Pelicula } from 'src/app/common/pelicula';
import { Sala } from 'src/app/common/sala';
import { Sede } from 'src/app/common/sede';
import { CineService } from 'src/app/services/cine-service';
import { FuncionService } from 'src/app/services/funcion-service';
import { PeliculaService } from 'src/app/services/pelicula-service';
import { SalaService } from 'src/app/services/sala-service';
import { SedeService } from 'src/app/services/sede-service';

@Component({
  selector: 'app-funciones-new',
  templateUrl: './funciones-new.component.html',
  styleUrls: ['./funciones-new.component.css']
})
export class FuncionesNewComponent implements OnInit {

  funcionNueva : Funcion = new Funcion() ;
  mensaje !: string ;
  peliculas : Pelicula[] | undefined; 
  cines : Cine[] | undefined; 
  cineSeleccionado : Cine = new Cine();
  sedeSeleccionado : Sede = new Sede();    
  sedes : Sede[] | undefined;
  salas : Sala[] | undefined;


  constructor(private funcionServicio : FuncionService , 
              private route: ActivatedRoute,
              private router: Router,
              private peliculaServicio : PeliculaService,
              private cineServicio : CineService,
              private sedeServicio : SedeService,
              private salaServicio : SalaService) { }
  
  crearFuncion()
  {
      console.log("Invocando servicio rest-postFuncion");
      console.log("Cine seleccionado : " + this.cineSeleccionado.razonSocial);
      this.funcionServicio.saveFuncion(this.funcionNueva).subscribe(data =>{
            console.log(data); 
            this.mensaje = "Se creo correctamente la funcion";   
         },
        error => {
             console.log(error);
              this.mensaje = "Hubo un error creandp la funcion :" + error;   
        }
        )  
    }  
  
  regresar()
    {
        console.log("A la busqueda de funciones de cine");
         this.router.navigate(['/funcionesWeb']);
    }
   
  cargarCines()
  {
    this.cineServicio.getCinesList().subscribe(
      data =>{
               console.log(data); 
               this.cines = data;
               
   },
   error => {
               console.log(error);
               this.mensaje = "No se pudo obtener los cines disponibles";   
    }
   )  
  } 
  cargarSedesPorCine()
  {
      this.sedeServicio.getSedesList(this.cineSeleccionado.idCine.toString()).subscribe(
        data =>{
                 console.log(data); 
                 this.sedes = data;
                 
     },
     error => {
                 console.log(error);
                 this.mensaje = "No se pudo obtener las sede disponibles";   
      }
     )  
  }
 
  cargarSalasPorSede()
  {

    this.salaServicio.getSalasList(this.sedeSeleccionado.idSede.toString()).subscribe(
      data =>{
               console.log(data); 
               this.salas = data;
               
   },
   error => {
               console.log(error);
               this.mensaje = "No se pudo obtener las salas disponibles";   
    }
   )  

  }

  ngOnInit(): void {
    this.mensaje = "";
    this.peliculaServicio.getPeliculasList().subscribe(
       data =>{
                console.log(data); 
                this.peliculas = data;
                
    },
    error => {
                console.log(error);
                this.mensaje = "No se pudo obtener las peliculas disponibles";   
     }
    )  
    this.cargarCines();
  } 
}
