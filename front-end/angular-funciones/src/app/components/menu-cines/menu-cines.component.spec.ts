import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCinesComponent } from './menu-cines.component';

describe('MenuCinesComponent', () => {
  let component: MenuCinesComponent;
  let fixture: ComponentFixture<MenuCinesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuCinesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuCinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
