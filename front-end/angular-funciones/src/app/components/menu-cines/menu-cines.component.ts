import { Component, OnInit } from '@angular/core';
import { Cine } from 'src/app/common/cine';
import { CineService } from 'src/app/services/cine-service';

@Component({
  selector: 'app-menu-cines',
  templateUrl: './menu-cines.component.html',
  styleUrls: ['./menu-cines.component.css']
})
export class MenuCinesComponent implements OnInit {

  cines : Cine[] | undefined; 


  constructor(private cineServicio : CineService) { }

  listarCines() {

    this.cineServicio.getCinesList().subscribe(
      data =>{
               console.log('Cine = ' + JSON.stringify(data)); 
               this.cines = data;
               
   },
   error => {
               console.log(error);
    }
   )  
  }



  ngOnInit(): void {
    this.listarCines();
  }

}
