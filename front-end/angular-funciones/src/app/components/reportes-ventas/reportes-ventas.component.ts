import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import { Reporte } from 'src/app/common/reporte';
import { VentaService } from 'src/app/services/venta-service';
import { ChartsModule } from 'ng2-charts';
  
@Component({
  selector: 'app-reportes-ventas',
  templateUrl: './reportes-ventas.component.html',
  styleUrls: ['./reportes-ventas.component.css']
})
export class ReportesVentasComponent implements OnInit {

  constructor(private ventasServicio: VentaService,
    private route: ActivatedRoute,
    private router: Router) { }

  reporteVentasFec: Reporte = new Reporte;

  //variables para el reporte de barras 
  barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  barChartLabels: Label[] = [];  //etiquetas eje x
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  public barChartPlugins = [];
  barChartData: ChartDataSets[] = [];  //valores eje y

  cargarReporteVentasPorFecha() {
    this.ventasServicio.getVentasReporte("3").subscribe(datos => {
      console.table(datos);
      this.reporteVentasFec = datos;
      this.barChartLabels = this.reporteVentasFec.etiquetas;
      this.barChartData = [
        { data: this.reporteVentasFec.valores, label: 'Ventas Por zonas' }
      ];
    })
  }
  cargarReporteVentasPorUsuario() { }

  cargarReporteVentasPorUbicacion() { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      this.cargarReporteVentasPorFecha();
      this.cargarReporteVentasPorUsuario();
      this.cargarReporteVentasPorUbicacion();
    });
  }

}
