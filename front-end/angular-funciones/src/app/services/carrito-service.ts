import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import { Funcion } from "../common/funcion";

@Injectable({
    providedIn: 'root'
  })
  
export class CarritoService {

    funcionesComprar : Funcion[] = [];
   
    precioTotal : Subject<number> = new BehaviorSubject<number>(0);

    cantidadTotal : Subject<number> = new BehaviorSubject<number>(0);

    constructor() { }
 
    agregarFuncion(nueva : Funcion)
    {
        this.funcionesComprar.push(nueva);
        this.calcularTotales();
    }

    calcularTotales()
    {
        let precioTotalVal: number = 0;
        let cantidadTotalVal: number = this.funcionesComprar.length;
        for (let item of this.funcionesComprar)
        {
            precioTotalVal += Number(item.precio.split("/")[1]);
        }
        // publicamos los nuevos valores ... todos los subscriptores a estas variables recibiran los nuevos valores.
       this.precioTotal.next(precioTotalVal);
       this.cantidadTotal.next(cantidadTotalVal);
    }
}
