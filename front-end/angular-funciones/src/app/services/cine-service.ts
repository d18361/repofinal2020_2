import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Cine } from "../common/cine";




@Injectable(
    {
      providedIn : 'root'
    }
)
export class CineService {

    private urlServicioRest = 'http://localhost:9078/carteleraWeb/rest/cines';

    constructor (private httpClient : HttpClient) {}
    
    getCinesList(): Observable<Cine[]> {
        return this.httpClient.get<Cine[]>(this.urlServicioRest);
        }
}
