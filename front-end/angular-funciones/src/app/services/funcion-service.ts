import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Funcion } from "../common/funcion";
import { map } from 'rxjs/operators';
import { Respuesta } from "../common/respuesta";
@Injectable(
    {
      providedIn : 'root'
    }
)
export class FuncionService {
   

    private urlServicioRest = 'http://localhost:9078/carteleraWeb/rest/funciones';
    private urlServicioRest2 =  'http://localhost:9078/carteleraWeb/rest/funciones/peliculas?opcion=2&'

    constructor (private httpClient : HttpClient) {}

    getFuncionesList(): Observable<Funcion[]> {
        return this.httpClient.get<Funcion[]>(this.urlServicioRest);
        }

    getFuncionByID(id: string | null ) : Observable<Funcion> {
      return this.httpClient.get<Funcion>(`${this.urlServicioRest}/${id}`); 
    }

    saveFuncion(data : Funcion | null ) : Observable<Respuesta>{
         // WorkAround : Declararlo de manera explicita.
          return this.httpClient.post<Respuesta>(this.urlServicioRest,data,{​​responseType: 'text' as 'json'}​​);
    }

    updateFuncion(id:string | null ,data : Funcion | null ) : Observable<Respuesta>{
      return this.httpClient.put<Respuesta>(`${this.urlServicioRest}/${id}`,data);
    }

    deleteFuncionByID(id : String | null) : Observable<Respuesta> {
      console.log("Invocando metodo de eliminacion");
      console.log(`${this.urlServicioRest}/${id}`);
      return this.httpClient.delete<Respuesta>(`${this.urlServicioRest}/${id}`); 
    }   

    getFuncionesPorCineList(idCine: string | null) :Observable<Funcion> {
      return this.httpClient.get<Funcion>(`${this.urlServicioRest}/cines/${idCine}`); 
    }

    getFuncionesPorPeliculaList(nombrePelicula:string | null) : Observable<Funcion> {

       //decodeURI funcion javascript para manejo de encoding y evitar errores de caracteres!!!
      return this.httpClient.get<Funcion>(decodeURI(`${this.urlServicioRest2}pelicula=${nombrePelicula}`)); 
    }
}  

  
