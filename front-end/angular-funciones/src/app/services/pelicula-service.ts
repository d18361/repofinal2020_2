import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Pelicula } from "../common/pelicula";


@Injectable(
    {
      providedIn : 'root'
    }
)
export class PeliculaService {

    private urlServicioRest = 'http://localhost:9078/carteleraWeb/rest/peliculas';

    constructor (private httpClient : HttpClient) {}
    
    getPeliculasList(): Observable<Pelicula[]> {
        return this.httpClient.get<Pelicula[]>(this.urlServicioRest);
        }
}
