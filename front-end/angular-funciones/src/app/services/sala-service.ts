import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Sala } from "../common/sala";

@Injectable(
    {
      providedIn : 'root'
    }
)
export class SalaService {

    
    private urlServicioRest = 'http://localhost:9078/carteleraWeb/rest/salas';

    constructor (private httpClient : HttpClient) {}
    
    getSalasList(idSede: string | null): Observable<Sala[]> {
        console.log(`${this.urlServicioRest}?sede=${idSede}`);
        return this.httpClient.get<Sala[]>(`${this.urlServicioRest}?sede=${idSede}`);
        }
}
