import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Sede } from "../common/sede";

@Injectable(
    {
      providedIn : 'root'
    }
)
export class SedeService {

    private urlServicioRest = 'http://localhost:9078/carteleraWeb/rest/sedes';

    constructor (private httpClient : HttpClient) {}
    
    getSedesList(idCine: string | null): Observable<Sede[]> {
        console.log(`${this.urlServicioRest}?cine=${idCine}`);
        return this.httpClient.get<Sede[]>(`${this.urlServicioRest}?cine=${idCine}`);
        }
}
  