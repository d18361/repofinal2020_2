import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Reporte } from "../common/reporte";

@Injectable(
    {
      providedIn : 'root'
    }
)
export class VentaService {

    private urlServicioRest =  'http://localhost:9078/carteleraWeb/rest/ventas/reportes';

    constructor (private httpClient : HttpClient) {}

    getVentasReporte(codOpcion: string | null): Observable<Reporte> {
        console.log(`${this.urlServicioRest}?opcion=${codOpcion}`);
        return this.httpClient.get<Reporte>(`${this.urlServicioRest}?opcion=${codOpcion}`);
        }
}
